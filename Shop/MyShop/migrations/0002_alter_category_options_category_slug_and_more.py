# Generated by Django 4.1.1 on 2022-09-21 14:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MyShop', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Категория', 'verbose_name_plural': 'Категории'},
        ),
        migrations.AddField(
            model_name='category',
            name='slug',
            field=models.SlugField(null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(max_length=200, unique=True, verbose_name='Категория'),
        ),
    ]
