# Generated by Django 4.1.1 on 2022-09-21 18:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MyShop', '0003_alter_category_slug_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='image',
            field=models.ImageField(default='default.jpg', upload_to='image/', verbose_name='Картинка'),
        ),
    ]
