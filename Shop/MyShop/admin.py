from django.contrib import admin
from .models import Category, Product, Commentary
from django.utils.safestring import mark_safe


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    list_display_links = ('name',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    list_filter = ('name',)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'get_image', 'price')
    list_display_links = ('name',)
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    list_filter = ('name', 'category__name')
    readonly_fields = ('get_image',)

    def get_image(self,obj):
        return mark_safe(f'<img src={obj.image.url} width="120" height="160"')

    get_image.short_description = "Картинка"


class CommentaryAdmin(admin.ModelAdmin):
    list_display = ('name', 'product', 'user', 'date')
    list_display_links = ('name', 'product')
    search_fields = ('user', 'product')
    list_filter = ('product__name', 'user__username')


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Commentary, CommentaryAdmin)
