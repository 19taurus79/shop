from django.urls import path

from . import views
app_name = 'MyShop'
urlpatterns = [
    path('', views.ProductView.as_view(), name='product_list'),
    path('register/', views.RegisterFormView.as_view(), name='register'),
    path('login/', views.LoginFormView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('search/', views.SearchResultsView.as_view(), name='search_results'),
    path('<slug:category_slug>/', views.ProductView.as_view(), name='product_list_by_category'),
    path('<int:id>/<slug:slug>', views.ProductDetail.as_view(), name='product_detail'),


]
