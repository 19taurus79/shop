from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import View, ListView, DetailView, CreateView, FormView, TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .forms import UserCreateForm, CommentForm
from Shop.cart.forms import CartAddProductForm


class ProductView(View):
    def get(self, request, category_slug=None):
        product = Product.objects.all()
        category = None
        categories = Category.objects.all()
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            product = product.filter(category=category)

        return render(request, 'product/list.html',
                      {'product_list': product, 'category': category, 'categories': categories,
                       'title': 'Главная страница'})


class ProductDetail(DetailView):
    def get(self, request, id, slug):
        prod = get_object_or_404(Product, id=id, slug=slug)
        comments = prod.comment.all()
        cart_product_form = CartAddProductForm()

        if request.method == "POST":
            form = CommentForm(request.POST)
            if form.is_valid():
                form = form.save(commit=False)
                form.user = request.user
                form.product = prod
                form.save()
                return redirect('product_detail', slug)
        else:

            form = CommentForm()

        return render(request, 'product/detail.html',
                      {'product': prod, 'comments': comments, 'form': form, 'cart_product_form': cart_product_form})


class RegisterFormView(FormView):
    form_class = UserCreateForm
    success_url = '/login/'
    template_name = 'product/register.html'

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)

    def form_invalid(self, form):
        return super(RegisterFormView,self).form_invalid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = 'product/login.html'
    success_url = '/'

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class SearchResultsView(ListView):
    model = Product
    template_name = 'product/search_results.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Product.objects.filter(Q(name__icontains=query))
        return object_list




