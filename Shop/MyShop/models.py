from django.db import models
from django.conf import settings
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=200, unique=True, verbose_name='Категория')
    slug = models.SlugField(max_length=200, unique=True, null=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('MyShop:product_list_by_category', args=[self.slug])


class Product(models.Model):
    name = models.CharField(max_length=200, unique=True, verbose_name='Товар')
    category = models.ForeignKey('Category', on_delete=models.CASCADE, verbose_name='Категория')
    description = models.CharField(max_length=1000, verbose_name='Описание')
    image = models.ImageField(verbose_name='Картинка', upload_to='image/', default='default.jpg')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Цена')
    slug = models.SlugField(max_length=200, unique=True, null=True)

    class Meta:
        ordering = ('category',)
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('MyShop:product_detail', args=[self.id, self.slug])


class Commentary(models.Model):
    name = models.CharField(max_length=500, verbose_name='Комментарий', null=True)
    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата')
    product = models.ForeignKey('Product', on_delete=models.CASCADE, verbose_name='Товар', related_name='comment')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Пользователь', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return self.name
